using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorResonance : MonoBehaviour
{
    public ResonanceType playerResonanceType;
    private float resonanceSpeed = 15.0f;
    private float rightBound = 7.0f;

    // Update is called once per frame
    void Update()
    {
        MoveResonance();
        //FireResonance(); ADD ADDITIONAL FUNCTIONALITY HERE
        if (this.gameObject.transform.position.x > rightBound)
        {
            DestroyResonance();
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Enemy"))
        {
            ResonanceType enemyResonance = collision.gameObject.GetComponent<ResonanceIndicator>().resonanceIndicator;
            if(playerResonanceType == enemyResonance)
            {
                Destroy(collision.gameObject);
            }
            DestroyResonance();
        }
    }

    private void MoveResonance()
    {
        this.gameObject.transform.Translate(Vector3.right * resonanceSpeed * Time.deltaTime);
    }

    private void DestroyResonance()
    {
        Destroy(this.gameObject);
    }
}
