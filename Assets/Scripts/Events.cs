using UnityEngine;
using UnityEngine.Events;

public class Events
{
    public class EventEnemyBreach : UnityEvent<GameState> { }
}
