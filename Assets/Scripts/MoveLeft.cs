using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour
{
    public float enemyMovementSpeed = 5.0f;
    // private GameManager gameManager; // TO USE FOR GAMESTATE OVER
    private float leftBound = -15.0f;   // Bound to destroy gameobject

    // _____ UNITY GAME LOOP _____
    void Start()
    {
        
    }

    void Update()
    {
        if(true)    // CHANGE TO GAMEOVER LATER FROM GAMEMANAGER
        {
            MoveEnemy();
        }
        
        if(this.gameObject.transform.position.x < leftBound)
        {
            DestroyEnemy();
            // RAISE EVENT FOR GAME MANAGER
        }

    }

    // _____ HELPER METHODS _____
    private void MoveEnemy()
    {
        this.gameObject.transform.Translate(Vector3.left * enemyMovementSpeed * Time.deltaTime);
    }

    private void DestroyEnemy()
    {
        Destroy(this.gameObject);
    }
}
