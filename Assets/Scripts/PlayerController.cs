using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResonanceType { PINK, GREEN, BLUE }

public class PlayerController : MonoBehaviour
{
    public float playerSpeed = 1.0f;

    public float xBoundLeft = -18.0f;
    public float xBoundRight = 12.0f;

    public GameObject[] resonancePrefabs;
    public Queue<ResonanceType> playerResonanceQueue = new Queue<ResonanceType>();

    private GameObject tmpResonance;
    private int maxStackSize = 3;

    void Update()
    {
        MovePlayer();
        ConstrainPlayerToBounds();
        StoreResonance();
        if(Input.GetKeyDown(KeyCode.Space))
        {
            FireResonance();
        }
        if(playerResonanceQueue.Count > 0)
        {
            Debug.Log("ACTIVE RESONANCE: " + playerResonanceQueue.Peek());
        }
    }

    void MovePlayer()
    {
        float verticalInput = Input.GetAxis("Vertical");
        //float horizontalInput = Input.GetAxis("Horizontal");

        //this.transform.Translate(Vector3.right * playerSpeed * horizontalInput * Time.deltaTime);
        this.transform.Translate(Vector3.forward * playerSpeed * verticalInput * Time.deltaTime);
    }

    void ConstrainPlayerToBounds()
    {
        if (this.transform.position.x < xBoundLeft)
        {
            this.transform.position = new Vector3(xBoundLeft, this.transform.position.y, this.transform.position.z);
        }
        else if (this.transform.position.x > xBoundRight)
        {
            this.transform.position = new Vector3(xBoundRight, this.transform.position.y, this.transform.position.z);
        }
    }

    void StoreResonance()
    {
        if(playerResonanceQueue.Count <= maxStackSize)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                playerResonanceQueue.Enqueue(ResonanceType.PINK);
                Debug.Log("[PlayerController.cs] QUEUEING PINK RESONANCE");
            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                playerResonanceQueue.Enqueue(ResonanceType.GREEN);
                Debug.Log("[PlayerController.cs] QUEUEING GREEN RESONANCE");
            }
            else if (Input.GetKeyDown(KeyCode.R))
            {
                playerResonanceQueue.Enqueue(ResonanceType.BLUE);
                Debug.Log("[PlayerController.cs] QUEUEING BLUE RESONANCE");
            }
        }
        else
        {
            Debug.Log("[PlayerController.cs] RESONANCE STACK IS FULL");
        }
    }

    void FireResonance()
    {
        if(playerResonanceQueue.Count > 0)
        {
            ResonanceType resonanceType = playerResonanceQueue.Dequeue();
            switch (resonanceType)
            {
                case ResonanceType.PINK:
                    tmpResonance = Instantiate(resonancePrefabs[(int)ResonanceType.PINK], this.gameObject.transform.position + Vector3.right, Quaternion.identity);
                    // INSERT CALL TO RESONANCE FUNCTION "FIRE"
                    break;
                case ResonanceType.GREEN:
                    tmpResonance = Instantiate(resonancePrefabs[(int)ResonanceType.GREEN], this.gameObject.transform.position + Vector3.right, Quaternion.identity);
                    break;
                case ResonanceType.BLUE:
                    tmpResonance = Instantiate(resonancePrefabs[(int)ResonanceType.BLUE], this.gameObject.transform.position + Vector3.right, Quaternion.identity);
                    break;
                default:
                    Debug.Log("[NO STORED RESONANCE TO FIRE");
                    break;
            }
        }
        else
        {
            Debug.Log("NO STORED RESONANCE TO FIRE");
        }
    }
}
