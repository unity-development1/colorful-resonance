using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] enemyPrefabs;
    private float xSpawnPosition = 10.0f;
    private float[] zSpawnPositions = { -2.25f, 2.25f, 6.5f };

    private float startDelay = 2.0f;
    private float repeatRate = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnEnemy", startDelay, repeatRate);
    }

    private void SpawnEnemy()
    {
        int spawnIndex = Random.Range(0, zSpawnPositions.Length);
        int randomEnemy = Random.Range(0, enemyPrefabs.Length);

        Vector3 spawnPosition = new Vector3(xSpawnPosition, 0.5f, zSpawnPositions[spawnIndex]);

        Instantiate(enemyPrefabs[randomEnemy], spawnPosition, enemyPrefabs[randomEnemy].gameObject.transform.rotation);
    }
}
